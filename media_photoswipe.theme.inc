<?php

/**
 * @file
 * Media PhotoSwipe theme functions.
 */

use Drupal\Component\Utility\Xss;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

/**
 * Prepares variables for media_photoswipe formatter templates.
 *
 * Default template: media-photoswipe-formatter.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An ImageItem object.
 *   - item_attributes: An optional associative array of html attributes to be
 *     placed in the img tag.
 *   - entity: An entity object.
 *   - settings: Formatter settings array.
 *
 * @codingStandardsIgnoreStart
 */
function template_preprocess_media_image_photoswipe_formatter(&$variables) {
  // @codingStandardsIgnoreEnd

  $item = $variables['item'];
  $item_attributes = $variables['item_attributes'] ?? [];
  $entity = $variables['entity'];
  $settings = $variables['settings'];
  $image_uri = $item->entity->getFileUri();
  $classes_array = ['media_photoswipe'];
  $data_pswp_img_attrs = [];

  // Build the caption.
  $entity_title = $entity->label();
  $entity_type = $entity->getEntityTypeId();

  switch ($settings['media_photoswipe_caption']) {
    case 'auto':
      // If the title is empty use alt or the entity title in that order.
      if (!empty($item->title)) {
        $caption = $item->title;
      }
      elseif (!empty($item->alt)) {
        $caption = $item->alt;
      }
      elseif (!empty($entity_title)) {
        $caption = $entity_title;
      }
      else {
        $caption = '';
      }
      break;

    case 'title':
      $caption = $item->title;
      break;

    case 'alt':
      $caption = $item->alt;
      break;

    case 'entity_title':
      $caption = $entity_title;
      break;

    case 'custom':
      $token_service = \Drupal::token();
      $caption = $token_service->replace(
        $settings['media_photoswipe_caption_custom'],
        [$entity_type => $entity, 'file' => $item],
        ['clear' => TRUE]
      );
      break;

    default:
      $caption = '';
  }

  // Shorten the caption for the example styles or when caption
  // shortening is active.
  $config = \Drupal::config('media_photoswipe.settings');
  $media_photoswipe_style = !empty($config->get('media_photoswipe_style')) ? $config->get('media_photoswipe_style') : '';
  $trim_length = $config->get('media_photoswipe_caption_trim_length');
  if (((strpos($media_photoswipe_style, 'media_photoswipe/example') !== FALSE) || $config->get('media_photoswipe_caption_trim')) && (strlen($caption) > $trim_length)) {
    $caption = substr($caption, 0, $trim_length - 5) . '...';
  }

  $gallery_id = \Drupal::service('media_photoswipe.gallery_id_generator')->generateId($entity, $item, $settings);

  // Set up the $variables['image'] parameter.
  if ($settings['style_name'] == 'hide') {
    $variables['image'] = [];
    $classes_array[] = 'js-hide';
  }
  elseif (!empty($settings['style_name'])) {
    $variables['image'] = [
      '#theme' => 'image_style',
      '#style_name' => $settings['style_name'],
    ];
  }
  else {
    $variables['image'] = [
      '#theme' => 'image',
    ];
  }

  if (!empty($variables['image'])) {
    $variables['image']['#attributes'] = $item_attributes;

    // Do not output an empty 'title' attribute.
    if (!empty($item->title)) {
      $variables['image']['#title'] = $item->title;
      $data_pswp_img_attrs['title'] = '"title":"' . $item->title . '"';
    }

    foreach (['width', 'height', 'alt'] as $key) {
      $variables['image']["#$key"] = $item->$key;
      if ($key == 'alt') {
        $data_pswp_img_attrs['alt'] = '"alt":"' . $item->alt . '"';
      }
    }

    $variables['image']['#uri'] = empty($item->uri) ? $image_uri : $item->uri;
  }

  if (!empty($settings['media_photoswipe_image_style'])) {
    $style = ImageStyle::load($settings['media_photoswipe_image_style']);
    $variables['url'] = $style->buildUrl($image_uri);
  }
  else {
    /** @var \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator */
    $file_url_generator = \Drupal::service('file_url_generator');

    $variables['url'] = $file_url_generator->generateAbsoluteString($image_uri);
  }

  // If File Entity module is enabled, load attribute values from file entity.
  if (\Drupal::moduleHandler()->moduleExists('file_entity')) {
    // File id of the save file.
    $fid = $item->target_id;
    // Load file object.
    $file_obj = File::load($fid);
    $file_array = $file_obj->toArray();
    // Populate the image title.
    if (!empty($file_array['field_image_title_text'][0]['value']) && empty($item->title) && $settings['media_photoswipe_caption'] == 'title') {
      $caption = $file_array['field_image_title_text'][0]['value'];
    }
    // Populate the image alt text.
    if (!empty($file_array['field_image_alt_text'][0]['value']) && empty($item->alt) && $settings['media_photoswipe_caption'] == 'alt') {
      $caption = $file_array['field_image_alt_text'][0]['value'];
    }
  }

  $variables['attributes']['title'] = Xss::filter($caption);
  $variables['attributes']['data-media-photoswipe-gallery'] = $gallery_id;
  $variables['attributes']['class'] = $classes_array;
  $variables['attributes']['data-pswp-width'] = $variables['image']['#width'];
  $variables['attributes']['data-pswp-height'] = $variables['image']['#height'];
  if (!empty($data_pswp_img_attrs)) {
    $variables['attributes']['data-pswp-img-attrs'] = '{' . implode(',', $data_pswp_img_attrs) . '}';
  }
}
