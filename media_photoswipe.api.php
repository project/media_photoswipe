<?php

/**
 * @file
 * API documentation for the Media PhotoSwipe module.
 */

/**
 * Allows to override PhotoSwipe settings and style.
 *
 * Implements hook_media_photoswipe_settings_alter().
 *
 * @param array $settings
 *   An associative array of PhotoSwipe settings.
 * @param string $style
 *   The name of the active style plugin. If $style is 'none', no PhotoSwipe
 *   theme will be loaded.
 *
 * @link
 *    https://photoswipe.com/getting-started PhotoSwipe documentation.
 * @endlink
 *   for the full list of supported parameters.
 *
 * @codingStandardsIgnoreStart
 */
function hook_media_photoswipe_settings_alter(&$settings, &$style) {
  // @codingStandardsIgnoreEnd
  // Disable automatic downscaling of images to maxWidth/maxHeight size.
  $settings['scalePhotos'] = FALSE;

  // Use custom style plugin specifically for node/123.
  if (current_path() == 'node/123') {
    $style = 'mystyle';
  }
}
