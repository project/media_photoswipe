/**
 * @file
 * Media PhotoSwipe JS.
 */

(function ($, Drupal, drupalSettings, once) {

  'use strict';

  var lightbox = new PhotoSwipeLightbox({
    gallery: '.photoswipe-gallery',
    children: 'a.media_photoswipe',
    wheelToZoom: TRUE,

    closeTitle: 'Close the dialog',
    zoomTitle: 'Zoom the photo',
    arrowPrevTitle: 'Go to the previous photo',
    arrowNextTitle: 'Go to the next photo',

    // dynamic import is not supported in UMD version
    pswpModule: PhotoSwipe
  });
  lightbox.init();

  Drupal.behaviors.initMediaPhotoSwipe = {
    attach: function (context, settings) {
      if (typeof settings.media_photoswipe === 'undefined') {
        return;
      }
    },
    /**
     * Triggers when user clicks on thumbnail.
     *
     * Code taken from http://photoswipe.com/documentation/getting-started.html
     * and adjusted accordingly.
     */
    onThumbnailsClick: function (e) {
      e = e || window.event;
      var $clickedGallery = $(this);
      var eTarget = e.target || e.srcElement;
      var $eTarget = $(eTarget);

      // find root element of slide
      var $clickedListItem = $eTarget.closest('.photoswipe');
      if (!$clickedListItem) {
        return;
      }

      // get the index of the clicked element
      var index = $clickedGallery.find('.photoswipe').index($clickedListItem);
      if (index >= 0) {
        e.preventDefault ? e.preventDefault() : e.returnValue = FALSE;
        // open PhotoSwipe if valid index found
        Drupal.behaviors.photoswipe.openPhotoSwipe(index, $clickedGallery);
        // Only prevent default when clicking on a photoswipe image.
        return FALSE;
      }
    },
  };

  // Create colorbox namespace if it doesn't exist.
  if (!Drupal.hasOwnProperty('media_photoswipe')) {
    Drupal.media_photoswipe = {};
  }

  /**
   * Global function to allow sanitizing captions and control strings.
   *
   * @param markup
   *   String containing potential markup.
   * @return @string
   *  Sanitized string with potentially dangerous markup removed.
   */
  Drupal.media_photoswipe.sanitizeMarkup = function (markup) {
    // If DOMPurify installed, allow some HTML. Otherwise, treat as plain text.
    if (typeof DOMPurify !== 'undefined') {
      var purifyConfig = {
        ALLOWED_TAGS: [
          'a',
          'b',
          'strong',
          'i',
          'em',
          'u',
          'cite',
          'code',
          'br'
        ],
        ALLOWED_ATTR: [
          'href',
          'hreflang',
          'title',
          'target'
        ]
      }
      if (drupalSettings.hasOwnProperty('dompurify_custom_config')) {
        purifyConfig = drupalSettings.dompurify_custom_config;
      }
      return DOMPurify.sanitize(markup, purifyConfig);
    }
    else {
      return Drupal.checkPlain(markup);
    }
  }

})(jQuery, Drupal, drupalSettings, once);
