<?php

namespace Drupal\media_photoswipe;

/**
 * An interface for checking if photoswipe should be active.
 */
interface ActivationCheckInterface {

  /**
   * Check if photoswipe should be activated for the current page.
   *
   * @return bool
   *   If photoswipe should be active.
   */
  public function isActive();

}
