<?php

namespace Drupal\media_photoswipe;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Installer\InstallerKernel;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * An implementation of PageAttachmentInterface for the photoswipe library.
 */
class MediaPhotoSwipeAttachment implements ElementAttachmentInterface {

  use StringTranslationTrait;

  /**
   * The service to determine if photoswipe should be activated.
   *
   * @var \Drupal\media_photoswipe\ActivationCheckInterface
   */
  protected $activation;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The PhotoSwipe settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * Create an instance of MediaPhotoSwipeAttachment.
   */
  public function __construct(ActivationCheckInterface $activation, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config) {
    $this->activation = $activation;
    $this->moduleHandler = $module_handler;
    $this->settings = $config->get('media_photoswipe.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable() {
    return !InstallerKernel::installationAttempted() && $this->activation->isActive();
  }

  /**
   * {@inheritdoc}
   */
  public function attach(array &$page) {
    if ($this->settings->get('custom.activate')) {
      $js_settings = [
        'transition' => $this->settings->get('custom.transition_type'),
        'speed' => $this->settings->get('custom.transition_speed'),
        'opacity' => $this->settings->get('custom.opacity'),
        'slideshow' => $this->settings->get('custom.slideshow.slideshow') ? TRUE : FALSE,
        'slideshowAuto' => $this->settings->get('custom.slideshow.auto') ? TRUE : FALSE,
        'slideshowSpeed' => $this->settings->get('custom.slideshow.speed'),
        'slideshowStart' => $this->settings->get('custom.slideshow.text_start'),
        'slideshowStop' => $this->settings->get('custom.slideshow.text_stop'),
        'current' => $this->settings->get('custom.text_current'),
        'previous' => $this->settings->get('custom.text_previous'),
        'next' => $this->settings->get('custom.text_next'),
        'close' => $this->settings->get('custom.text_close'),
        'overlayClose' => $this->settings->get('custom.overlayclose') ? TRUE : FALSE,
        'returnFocus' => $this->settings->get('custom.returnfocus') ? TRUE : FALSE,
        'maxWidth' => $this->settings->get('custom.maxwidth'),
        'maxHeight' => $this->settings->get('custom.maxheight'),
        'initialWidth' => $this->settings->get('custom.initialwidth'),
        'initialHeight' => $this->settings->get('custom.initialheight'),
        'fixed' => $this->settings->get('custom.fixed') ? TRUE : FALSE,
        'scrolling' => $this->settings->get('custom.scrolling') ? TRUE : FALSE,
        'mobiledetect' => $this->settings->get('advanced.mobile_detect') ? TRUE : FALSE,
        'mobiledevicewidth' => $this->settings->get('advanced.mobile_device_width'),
      ];
    }
    else {
      $js_settings = [
        'opacity' => '0.85',
        'current' => $this->t('{current} of {total}'),
        'previous' => $this->t('« Prev'),
        'next' => $this->t('Next »'),
        'close' => $this->t('Close'),
        'maxWidth' => '98%',
        'maxHeight' => '98%',
        'fixed' => TRUE,
        'mobiledetect' => $this->settings->get('advanced.mobile_detect') ? TRUE : FALSE,
        'mobiledevicewidth' => $this->settings->get('advanced.mobile_device_width'),
      ];
    }

    $style = $this->settings->get('custom.style');

    // Give other modules the possibility to override Media PhotoSwipe
    // settings and style.
    $this->moduleHandler->alter('media_photoswipe_settings', $js_settings, $style);

    // Add PhotoSwipe js settings.
    $page['#attached']['drupalSettings']['media_photoswipe'] = $js_settings;

    // Add and initialise the Media PhotoSwipe plugin.
    $page['#attached']['library'][] = 'media_photoswipe/media_photoswipe';

    // Add JS and CSS based on selected style.
    if ($style != 'none') {
      $page['#attached']['library'][] = "media_photoswipe/$style";
    }
    else {
      $page['#attached']['library'][] = "media_photoswipe/init";
    }
  }

}
